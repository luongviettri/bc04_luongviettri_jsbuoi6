function createTags() {

    var divChan = `<div class="bg-danger text-light py-2 border rounded px-2">Đây là div chẵn</div>`
    var divLe = `<div class="bg-primary text-light py-2 border rounded px-2">Đây là div lẻ</div>`

    var resultEl = document.getElementById("result");

    var htmlTags = "";

    for (var i = 0; i < 10; i++) {
        if ((i + 1) % 2 == 0) {
            htmlTags += divChan;
        } else {
            htmlTags += divLe;
        }
    }
    resultEl.innerHTML = htmlTags;


}